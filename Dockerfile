# Verwenden der neuesten Ubuntu-Version als Basis
FROM ubuntu:latest

ENV TZ=Europe/Zurich
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update 

RUN apt install -y openjdk-21-jdk 
RUN apt install -y openjdk-21-jre-headless

RUN apt install -y git

# RUN apt-get install -y texlive-full
RUN apt install -y texlive-latex-base
RUN apt install -y texlive-latex-recommended
RUN apt install -y texlive-pictures
RUN apt install -y texlive-latex-extra
RUN apt install -y texlive-bibtex-extra
RUN apt install -y texlive-fonts-extra
RUN apt install -y texlive-fonts-recommended
RUN apt install -y texlive
RUN apt install -y texlive-plain-generic
RUN apt install -y texlive-full

RUN apt install -y texlive-lang-german

## texlive-latex-base            59 MB      216 MB
## texlive-latex-recommended     74 MB      248 MB
## texlive-pictures              83 MB      277 MB
## texlive-fonts-recommended     83 MB      281 MB
## texlive                       98 MB      314 MB
## texlive-plain-generic         82 MB      261 MB
## texlive-latex-extra          144 MB      452 MB
## texlive-full                2804 MB     5358 MB

# ermöglicht das deploy auf einen ftp Server (gibb share)
RUN apt install -y python3-paramiko

# Kopieren der binaries in den Docker-Container
COPY runme.sh /usr/local/bin/runme.sh
RUN chmod +x /usr/local/bin/runme.sh
COPY asciiDoc-1.0-SNAPSHOT-jar-with-dependencies.jar /usr/local/bin/asciiDoc-1.0-SNAPSHOT-jar-with-dependencies.jar
COPY a2tex /usr/local/bin/a2tex

# smartlearn Latex
RUN mkdir -p $(kpsewhich --var-value TEXMFHOME)/tex/latex/
RUN cd $(kpsewhich --var-value TEXMFHOME)/tex/latex/
RUN git clone https://git.gibb.ch/kurt.jaermann/latexsmartlearnvorlage.git $(kpsewhich --var-value TEXMFHOME)/tex/latex/

# Setzen des Arbeitsverzeichnises
WORKDIR /data

# Ausführen des "runme", wenn der Container gestartet wird
ENTRYPOINT ["runme.sh"]

# -----------
# docker build -t a2tex .
# docker run -it --rm -v $PWD:/data -e GIT_TOKEN=glpat-hZjWLrtKL_xizDSGJ_HP a2tex bash

# docker run --rm -v $PWD:/data a2tex m324Auftrag_4_Erprobung.tex

# docker run -it a2tex bash
# mit alles auf dem m324_kurs repo : glpat-hZjWLrtKL_xizDSGJ_HP
# docker run -it --rm -v $PWD:/data -e GIT_TOKEN=glpat-hZjWLrtKL_xizDSGJ_HP a2tex bash

# docker build -t a2tex .
# docker run --rm -v $PWD:/data a2tex m324Auftrag_4_Erprobung.tex



